
function plot_gradient_paths_plane(m,a,b,c,col,X,Y,Z)
    list = [X,Y,Z];
    basis=[b-a;c-a]*m;

    P = cross(basis(1,:),basis(2,:));
    P = P./norm(P);
    
    on_plane = abs(sum((list-c*m).*P,2))<0.5;

%     basis=basis./vecnorm(basis,2,2); 
    plane_pos = (list-a*m)/basis;
    within_plane = all(all((list-a*m)/basis>0,2) & all((list-a*m)/basis<1,2),2);
%     
    plane_pos(~(on_plane & within_plane),:) = NaN;
    tmp=a*m+ plane_pos*basis; 
    plot3(tmp(:,1),tmp(:,2),tmp(:,3),'Color',col,'LineWidth',0.5,'Marker','none');
%     plot3(X(on_plane & within_plane),Y(on_plane & within_plane),Z(on_plane & within_plane),'Color',col,'LineWidth',1);
    plot3(X,Y,Z,'Color','r','LineWidth',1);

end
