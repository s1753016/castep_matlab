
function plot_unit_cell(m)
    %% Unit Cell Edges
    ed = [0 0 0; 0 0 1; 0 1 1; 1 1 1; 1 0 1; 0 0 1;]*m;
    plot3(ed(:,1),ed(:,2),ed(:,3),'-k')
    ed = [0 0 0; 0 1 0; 0 1 1]*m;
    plot3(ed(:,1),ed(:,2),ed(:,3),'-k')
    ed = [0 0 0; 1 0 0; 1 1 0;0 1 0]*m;
    plot3(ed(:,1),ed(:,2),ed(:,3),'-k')
    ed = [1 0 0; 1 0 1]*m;
    plot3(ed(:,1),ed(:,2),ed(:,3),'-k')
    ed = [1 1 0; 1 1 1]*m;
    plot3(ed(:,1),ed(:,2),ed(:,3),'-k')
end

