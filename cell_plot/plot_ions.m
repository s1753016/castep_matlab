
function h = plot_ions(m,Atom_n,r_cart)
global atom_colors atom_sizes res
[spha, sphb, sphc] = sphere(res);
atom_coords=r_cart*m;


for i =1:length(Atom_n)
    r = atom_sizes(Atom_n(i));
    h(i).surf = surf(r.*spha+atom_coords(i,1),r.*sphc+atom_coords(i,2)...
        ,r.*sphb+atom_coords(i,3),'LineStyle','none');
    set(h(i).surf,'FaceColor',atom_colors(Atom_n(i),:), ...
        'FaceLighting','gouraud','EdgeColor','none','AmbientStrength',0.3...
        ,'DiffuseStrength',0.8,'SpecularStrength',0.9,'SpecularExponent',25);

    h(i).ID.Atom_n = Atom_n(i);
    h(i).ID.Atom_n = atom_coords(i,:);
end

end
