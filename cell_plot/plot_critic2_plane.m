
function plot_critic2_plane(m,a,seed,col,alp)

plot_plane(m,a(1:3),a(4:6),a(7:9),col,alp)
iso1 = import_iso([seed '.iso']);
for i = 1:length(iso1)
    plot_line_on_plane(m,a(1:3),a(4:6),a(7:9),[0 0.4667 0],iso1(i).iso(:,1),iso1(i).iso(:,2));
end
grad1 = import_grad_path([seed '.dat']);
for i = 1:length(grad1)
    plot_gradient_paths_plane(m,a(1:3),a(4:6),a(7:9),[0 0 0],grad1(i).dat(:,3),grad1(i).dat(:,4),grad1(i).dat(:,5));
end

end
