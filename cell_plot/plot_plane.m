
function  plot_plane(m,a,b,c,col,alpha)
    corners = [a;b;b+c-a;c]*m;
    p = patch(corners(:,1),corners(:,2),corners(:,3),col,'Facealpha',alpha,...
         'FaceLighting','gouraud','AmbientStrength',0.75 ...
        ,'DiffuseStrength',0.8,'SpecularStrength',0.9,'SpecularExponent',25);
end
