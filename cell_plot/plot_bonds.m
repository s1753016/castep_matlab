
function plot_bonds(m,Atom_n,r_cart,species1,species2,rmin,rmax,style,n_cells)
global atom_colors u_Atom res

if n_cells>0
    r_cart_ss=[];
    for i=-n_cells:n_cells
        for j=-n_cells:n_cells
            for k=-n_cells:n_cells
                r_cart_ss = [r_cart_ss; r_cart + [i,j,k]];
            end
        end
    end
    atom_coords    = r_cart   *m;
    atom_coords_ss = r_cart_ss*m;
    Atom_n_ss = repmat(Atom_n,1,(2.*n_cells+1).^3);

    atom_coords1 = atom_coords(Atom_n==find(u_Atom==species1),:);
    atom_coords2 = atom_coords_ss(Atom_n_ss==find(u_Atom==species2),:);
else
    atom_coords    = r_cart   *m;
   
    atom_coords1 = atom_coords(Atom_n==find(u_Atom==species1),:);
    atom_coords2 = atom_coords(Atom_n==find(u_Atom==species2),:);
end  
    
atom_colors1 = atom_colors(u_Atom==species1,:);
atom_colors2 = atom_colors(u_Atom==species2,:);

for i =1:size(atom_coords1,1)
    for j = 1:size(atom_coords2,1)
        vec = atom_coords1(i,:)-atom_coords2(j,:);
        dist = sum(vec.^2);
        if dist<rmax.^2 && dist>rmin.^2
            if strcmp(style,'cylinder') 
                cylinder_bond(vec,0.07,sqrt(dist),atom_coords1(i,:),atom_coords2(j,:));
            else
                line_bond(atom_coords1(i,:),atom_coords2(j,:),style);
            end
            
            if n_cells>0
                plot_ions(m,u_Atom(u_Atom==species2),atom_coords2(j,:)/m)
            end
        end
    end
end    
    function cylinder_bond(vec,r,l,centre1,centre2)
        [cyla, cylb, cylc] = cylinder2(r,-vec,res,0.5*l);
        hsurf = surf(cyla+centre1(1),cylb+centre1(2) ...
            ,cylc+centre1(3),'LineStyle','none');
        set(hsurf,'FaceColor',atom_colors1, ...
            'FaceLighting','gouraud','EdgeColor','none','AmbientStrength',0.3...
        ,'DiffuseStrength',0.8,'SpecularStrength',0.9,'SpecularExponent',25);
        [cyla, cylb, cylc] = cylinder2(r,vec,100,0.5*l);
        hsurf = surf(cyla+centre2(1),cylb+centre2(2) ...
            ,cylc+centre2(3),'LineStyle','none');
        set(hsurf,'FaceColor',atom_colors2, ...
            'FaceLighting','gouraud','EdgeColor','none','AmbientStrength',0.3...
        ,'DiffuseStrength',0.8,'SpecularStrength',0.9,'SpecularExponent',25);
    end

    function line_bond(centre1,centre2,style)
        plot3([centre1(1) centre2(1)],[centre1(2) centre2(2)],[centre1(3) centre2(3)],style,'color',atom_colors1,'LineWidth',2);
    end
end
