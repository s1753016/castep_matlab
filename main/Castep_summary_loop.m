% Produces .summ files for a *.castep files in a given directory. '.Summ'
% files contain enthalpy, volume and stoichiometric data contained in large
% .castep files
% 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

function Castep_summary_loop(directory)

files = dir(directory);

for file = files'
    Castep_summary([file.folder '/' file.name])
end

end
