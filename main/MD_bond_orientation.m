function MD_bond_orientation(filename,species1,species2,ts,st,en)
    clear textprogressbar
    %% Setup
    if nargin < 6
	    st=0;en=1;
    end
    tic
    [th,ph,r,m,Atom] = import_md_bonds(filename,species1,species2,ts,st,en);
    %r
    toc
    if size(r,1)<20
        warning('Data not very long\n')
    end
    
    % Convert to degrees
    th = th.*(180/pi);
    ph = ph.*(180/pi);
    ph=ph+90;

    % Cell Volume
    vol = abs(det(m));
    % Numbers 
    Na=sum(Atom==species1);
    Nb=sum(Atom==species2);
    N=Na+Nb;
    % Density
    rho=N/vol;
    
    % Bin Width
    bw=0.01;

    %% Radial Distribution
    % Make Histogram
    [h,h2] = histcounts(r(:), 'BinWidth',bw);%,'BinLimits',[0 10]);
    % Normalise per timestep
    h    = h./size(r,1);
    % Get g(r) normalised correctly
    gofr = N*h./(rho*Na*Nb*4*pi*h2(2:end).^2*bw);
    
    % Get n(r) normalised correctly
    nofr = (Nb/(Na+Nb))*4*pi*rho*bw*cumsum(gofr.*h2(2:end).^2);


    %% Get X-Y-X bond angles
    angle = zeros(size(h2));
    % Take the first 10 bonds
    tmpn = 10;
    % Which makes 10 choose 2 bond pairs 
    tmpcm=nchoosek(10,2);
    % Only calculate for bonds less than 10 Angstrom
    maxr = 10;
    % Allocate output
    out =nan.*ones(size(r,1),size(r,2),tmpcm);
    rmax=nan.*ones(size(r,1),size(r,2),tmpcm);

    textprogressbar(sprintf('Getting %s-%s-%s bond angles : ',species2,species1,species2));
    tic
    for m =1:size(out,1)
        textprogressbar(round(100*(m./size(out,1))))
        for l = 1:size(out,2)
            tmpc=0;
            for i = 1:tmpn
                for j = i+1:tmpn
                    if tmpc==tmpcm
                        error('tmpcm too small')
                    end
                    if max(r(m,l,i),r(m,l,j))<maxr
                        tmpc = tmpc+1;
                        out(m,l,tmpc) = distance(ph(m,l,i)-90,th(m,l,i),ph(m,l,j)-90,th(m,l,j));
                        rmax(m,l,tmpc) = max(r(m,l,i),r(m,l,j));
                    end
                end
            end
        end
    end
    textprogressbar('Done')
    toc
   
    % Find average for rmax < r
    for m = 2:size(h2,2)
        angle_std(m)=  std(out(rmax<h2(m)),'omitnan');
        angle(m)    = mean(out(rmax<h2(m)),'omitnan');
        hold on
    end

    % Write: r g(r) n(r)
    fid=fopen([filename(1:end-4) '-' species1 '-' species2 '_gofr_' num2str(ts) '-' num2str(st) '-' num2str(en) '.dat'],'w');
    fprintf(fid,' %f %f %f\n',[h2(2:end); gofr; nofr;]);
    fclose(fid);
    
    % Write: r theta(r) d_theta(r)
    fid=fopen([filename(1:end-4) '-' species1 '-' species2 '_angles_' num2str(ts) '-' num2str(st) '-' num2str(en)  '.dat'],'w');
    fprintf(fid,' %f %f %f\n',[h2(2:end); angle(2:end); angle_std(2:end)]);
    fclose(fid);

end
