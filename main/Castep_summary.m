
function Castep_summary(file)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    tmp = split(file,{'.castep','/'})
    name = cell2mat(tmp(end-1));
    directory = split(file,name);
    directory= cell2mat(directory(1));


    [~,b] = grep('-s','Enth', file);
    Enth =     sscanf(b.match{end},' BFGS: Final Enthalpy     = %f eV');
    if isempty(Enth)
        Enth = sscanf(b.match{end},' LBFGS: Final Enthalpy     = %f eV');
    end
    [~,b] = grep('-s','number of ions', file);
    n_ions = sscanf(b.match{1},'                          Total number of ions in cell =   %d');
    [~,b] = grep('-s','volume', file);
    vol = textscan(b.match{end},'                       Current cell volume = %f       A**3');
    vol = cell2mat(vol);
    
    t = textread(file,'%s','delimiter','\n','bufsize',10*8190);
    pos1 = find(strcmpi(t, 'x----------------------------------------------------------x'));
    pos1=pos1(1);
    pos2=find(strcmpi(t, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'));
    pos2=pos2(2);
    


    
    species = {};
    stoich = [];
    
    for j = pos1+1:pos1+n_ions
         s = sscanf(t{j},'x  %s            %d         %f %f %f   x ');
%          t{j}
         if ~ismember(t{j}(4:5),species)
            species = categorical([species t{j}(4:5)]);
            stoich = [stoich 0];
         end
         stoich = stoich + double(species==t{j}(4:5));
         
    end
    
    fid = fopen([directory name '.summ'],'w');
    
    fprintf(fid,'%s \n---------------------\nEnth   %f \nN_ions %d \nVol    %d\n', name, Enth, n_ions,vol);
%     for i = 1:length(species)
%         fprintf(fid,'%s      %d \n', species(i), stoich(i));
%     end
    fprintf(fid,'%s ',species);
    fprintf(fid,'\n');
    fprintf(fid,'%d ',stoich);
    fprintf(fid,'\n---------------------');
    fclose(fid);
end






% %% Import files
% files = dir([directory  num2str(P) '/*.castep']);
% 
% for i = 1:length(files)
% %     [files(i).folder '/' files(i).name]
% 
%     rootname=strrep(files(i).name,['-' num2str(P) '.castep'],'');
% 
%     % Get Enthalpy
%     [~,b] = grep('-s','Enth', [files(i).folder '/' files(i).name]);
%     files(i).Enth = sscanf(b.match{end},' BFGS: Final Enthalpy     = %f eV');
%     
%     % Get TS
%     if addTS
%         
%         disp([directory 'TS/' rootname '_TS.dat'])
%         TS = import_TS2([directory 'TS/2019/' rootname '_TS.dat']);
%         TS = table2array(TS);
%         a = find(TS(:,1)==P);
%         b = find(TS(1,:)==T);
% 
%         if ~isempty(a+b)
%             TS_add(i) = TS(a,b);
%         else
%             TS_add(i) = NaN;
%         end
%     else
%         TS_add(i) = 0;
%     end
% 
%     
%     files(i).Enth = files(i).Enth + TS_add(i).*addTS;
%     % Get number of ions
%     [~,b] = grep('-s','number of ions', [files(i).folder '/' files(i).name]);
%     files(i).n_ions = sscanf(b.match{1},'                          Total number of ions in cell =   %d');
%     
%     % Get stochiometry - counts number of appearances of each species
%     % (slow,bad)
%     
%     t = textread([files(i).folder '/' files(i).name],'%s','delimiter','\n');
%     
%     pos1 = find(strcmpi(t, 'x----------------------------------------------------------x'));
%     pos1=pos1(1);
%     
% %     a = get_castep_atoms([files(i).folder '/' files(i).name], pos1+1, pos1+files(i).n_ions+1);
%     files(i).stoich = zeros(size(species));
%     
%     for j = pos1+1:pos1+files(i).n_ions
%          s = sscanf(t{j},'x  %s            %d         %f %f %f   x ');
%          files(i).stoich = files(i).stoich + double(species==char(s(1)));
%          
%          
%     end
%     
%     
% %     for j = 1:length(species)
% %         [a,b] = grep('-s',['* ' char(species(j))], [files(i).folder '/' files(i).name]);
% %         b.lcount(isempty(b.lcount)) = 0;
% %         files(i).stoich(j) = b.lcount;
% %     end
%     
%     files(i).stoich = files(i).n_ions.*files(i).stoich./sum(files(i).stoich);
%     
%     % linear algebra to transform to basis
%     [tmp] = linsolve(basis',files(i).stoich');
%     files(i).n=tmp(1);
%     files(i).m=tmp(2);
%     files(i).x=tmp(1)./sum(tmp);
%     % Enthalpy per formula unit
%     files(i).Enth_fu = files(i).Enth./sum(tmp);
%     
%     % Greatest common divisor ensures correct formula unit ie. 
%     % C2H6 -> CH3
%     % Not actually used in convex hull - just for outputting normalised
%     % ZPEs
%     
%     fu = gcd(files(i).n,files(i).m);
%     TS_add(i) = TS_add(i)./fu;
%     
%     % Print
%     fprintf('%s %f %d \n', files(i).name, TS_add(i), fu);
%     name{i} = strrep(files(i).name,'.castep','');
%     
% end
% 
% cells = struct2cell(files);
% 
% %% Check we have at least one input for each endpoint
% stoichs = cell2mat(cells(12,:));
% Enths_fu= cell2mat(cells(13,:));
% 
% if min(stoichs)~=0 || max(stoichs)~=1; disp('At least one endpoint missing'); DH=NaN;x=NaN;k=NaN; return; end
% 
% %% Compare with Baseline
% 
% H_n = min(Enths_fu(stoichs==1));
% H_m = min(Enths_fu(stoichs==0));
% 
% for i=1:length(files)
%     DH(i) = (files(i).Enth - files(i).n*H_n - files(i).m*H_m)./(files(i).n+files(i).m);
%     x(i) = files(i).x;
% end
% 
% % Finally get convex hull 
% k = convhull(x ,DH, 'simplify', true);
%         
% 
% end