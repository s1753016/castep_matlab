%  Builds convex hulls to compare preexisting enthalpy data (red) with new
%  search data (black). Searches were done at 100 and 200 GPa and relevent
%  directories exist in './geom_opt_searches/', the full set of data is in
%  './geom_opt/'. This produces figure A2 in L. J. Conway, Geosciences,
%  2019.
% 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %



% figure(3)
close all
T=1500;
colours=linspecer(6);
% Define end points of convex hull
species = categorical({'C' 'H'});
% basis = [1 1 0; 0 3 1];
basis = [1 4 ; 0 2];
for P = [19 20 21]

    
    % Plot
    figure(P+1)
    hold on
    
    % directory='phonons/full/'
    directory='castep-out-TS/';
%     directory='NH4F-HF-TS/';
%     directory='castep-out/';

    
    % [DH, x, k,name] = Custom_Hulls(directory,species,basis)
    [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,0);
    DH=DH1;
    hull_p=[(DH(k)<=0).*x(k);(DH(k)<=0).*(DH(k))]';
    plot(hull_p(:,1),hull_p(:,2),'-ko','HandleVisibility','on','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','k')
    scatter(x,DH,15,'Marker','o','MarkerEdgeColor',colours(1,:),'MarkerFaceColor',colours(1,:),...
        'MarkerFaceAlpha',1,'MarkerEdgeAlpha',1);
%     T=0;
%     [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,1);
%     DH=DH1;
%     scatter(x,DH,15,'Marker','o','MarkerEdgeColor',colours(2,:),'MarkerFaceColor',colours(2,:),...
%         'MarkerFaceAlpha',0.8,'MarkerEdgeAlpha',0);
%     plot((DH(k)<=0).*x(k),(DH(k)<=0).*(DH(k)),'-k','HandleVisibility','off')
%     T=1000;
%     [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,1);
%     DH=DH1;
%     scatter(x,DH,15,'Marker','o','MarkerEdgeColor',colours(3,:),'MarkerFaceColor',colours(3,:),...
%         'MarkerFaceAlpha',0.8,'MarkerEdgeAlpha',0);
%     plot((DH(k)<=0).*x(k),(DH(k)<=0).*(DH(k)),'-k','HandleVisibility','off')
%     T=1500;
%     [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,1);
%     DH=DH1;
%     scatter(x,DH,15,'Marker','o','MarkerEdgeColor',colours(4,:),'MarkerFaceColor',colours(4,:),...
%         'MarkerFaceAlpha',0.8,'MarkerEdgeAlpha',0);
%     plot((DH(k)<=0).*x(k),(DH(k)<=0).*(DH(k)),'-k','HandleVisibility','off')
%     [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,0);
%     DH=DH1;
%     scatter(x,DH,10,'Marker','o','MarkerEdgeColor',[0.2941    0.5447    0.7494],'MarkerFaceColor',[0.2941    0.5447    0.7494],...
%         'MarkerFaceAlpha',0.5,'MarkerEdgeAlpha',0.5);
%     plot((DH(k)<=0).*x(k),(DH(k)<=0).*(DH(k)),'-k')
    
%     directory='castep-out_TS/';
%     [DH1, x, k,~,name,stoich] = Custom_Hulls_TS_summ(directory,P,T,species,basis,0);
%     DH=DH1;
%     scatter(x,DH,10,'Marker','o','MarkerEdgeColor',colours(2,:),'MarkerFaceColor',colours(2,:),...
%         'MarkerFaceAlpha',0.5,'MarkerEdgeAlpha',0);
%     plot((DH(k)<=0).*x(k),(DH(k)<=0).*(DH(k)),'-k')
    
    for xx = [1 1/3 2/5 1/2 2/3 0] %[1/2, 2/3, 3/4,4/5, 5/6,8/9, 1] %[0 1/4 1/3 1/2 2/3 3/4 1]
        name_xx=name(x==xx);
        text(x(x==xx),DH(x==xx),name_xx,'Interpreter','none','FontSize',18,'Color','r')
        tmp = 1:length(DH(x==xx));
        tmp2 = sortrows([tmp', DH(x==xx)'],2);
        tmp3 = tmp2(1:end,1);
        for n = name_xx(tmp3)
            fprintf('*/%s.cell ', n{:}); 
        end
        try
%         system(['open -a vesta cells2/*/' name_xx{tmp3(1)} '.cell']);
%         system(['mkdir -p  NH4F-HF-TS/' num2str(P)]);
% %         system(['cp cells2/*/' name_xx{tmp3(1)} '.cell' ' best/' num2str(P)]);
%         system(['cp castep-out-TS/' num2str(P) '/' name_xx{tmp3(1)} '.castep' ' NH4F-HF-TS/' num2str(P)]);
        catch
            
        end
    end
%     fprintf('\n ', n{:}); 


    %% figure gaff
    text(0.95,0.8,[num2str(P) ' GPa'],'HorizontalAlignment','right','Interpreter','Latex','Units','normalized')
    xlabel('(H$_2$)$_{x}$(H$_2$O)$_{(1-x)}$','Interpreter','Latex')
    ylabel('$\Delta H_f$ (eV/molecule)','Interpreter','Latex')
    box on
    set(gca,'FontSize',10);
    set(gca,'TickLabelInterpreter','latex')
    set(gca,'LabelFontSizeMultiplier',0.8)
    box on
    ylim([-0.05 0.05])
    xlim([0 1])
    set(gca,'XTick',[0:0.2:1])
    set(gca,'YTick',[-0.05:0.025:0.05])
    ytickformat('%.2f')
    set(gcf,'PaperUnits','points');
    pos = get(gcf,'position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','points','PaperSize',[200, 100],'position',[pos(1) pos(2) 200 100])
    box on
    set(gca,'units','points','Position',[40 35 150 50]);
%     set(gca,'XTickLabel',[],'YTickLabel',[],'YLabel',[],'XLabel',[])
    print(gcf,['out_NH4F-HF_' num2str(10*P)], '-dpdf')
end
% xlabel('(HF)$_{x}$(NH$_3$)$_{(1-x)}$','Interpreter','Latex')
xlabel('(HF)$_{x}$(NH$_4$F)$_{(1-x)}$','Interpreter','Latex')

ylabel('$\Delta H_f$ (eV/molecule)','Interpreter','Latex')
% set(gca,'XTickLabel',[0:0.2:1],'YTick',[-0.2:0.1:0.2],'YTickLabel',[-0.2:0.1:0.2])
ytickformat('%.2f')

print(gcf,['out_NH4F-HF_' num2str(10*P)], '-dpdf')
