
function [DH, x, k,name,stoichs] = Custom_Hulls_summ(directory,species,basis)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build Convex Hulls with custom end points
%
% Give all N species as a catagory 
% Then fill basis as 2 by N matrix for end points
% eg. n(H2O):m(CH4)
% species = categorical({'H' 'O' 'C'});
% basis = [2 1 0; 4 0 1];
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(species)~=length(basis); disp('Error: Check end point inputs'); return; end

%% Import files
files = dir([directory '/*.summ']);

for i = 1:length(files)
    [files(i).folder '/' files(i).name];
    % Read in .summ file
    t = textread([files(i).folder '/' files(i).name],'%s','delimiter','\n');
    % Get Enthalpy and number of ions
    files(i).Enth = sscanf(t{3},'Enth %f');
    files(i).n_ions = sscanf(t{4},'N_ions %d');
    
    % Get Stoichiometry in correct basis
    for j = 1:length(species)
        tmp1 = split(t{6});
        tmp2 = split(t{7});
        if sum(tmp1 == string(species(j)))==1
            files(i).stoich(j)=str2double(tmp2(tmp1==string(species(j))));
        else
            files(i).stoich(j) = 0;
        end
    end
    % normalise stoirchiometry vector
    files(i).stoich = files(i).n_ions.*files(i).stoich./sum(files(i).stoich);
    
    % linear algebra to transform to basis
    [tmp] = linsolve(basis',double(files(i).stoich'));
    % Solved to within a tolerance
    tmp(abs(tmp)<10^-10)=0;
    tmp(abs(tmp-1)<10^-10)=1;
    % Get X_n Y_m
    files(i).n=tmp(1);
    files(i).m=tmp(2);
    % Get X_x Y_{1-x}
    files(i).x=tmp(1)./sum(tmp);
    % Enthalpy per formula unit
    files(i).Enth_fu = files(i).Enth./sum(tmp);
    
    name{i} = strrep(files(i).name,'.summ','');
    
end

cells = struct2cell(files);

% Check we have at least one input for each endpoint
stoichs = cell2mat(cells(12,:));
Enths_fu= cell2mat(cells(13,:));

if min(stoichs)~=0 || max(stoichs)~=1; disp('At least one endpoint missing'); DH=NaN;x=NaN;k=NaN; return; end

% Compare with Baseline

H_n = min(Enths_fu(stoichs==1));
H_m = min(Enths_fu(stoichs==0));

for i=1:length(files)
    DH(i) = (files(i).Enth - files(i).n*H_n - files(i).m*H_m)./(files(i).n+files(i).m);
    x(i) = files(i).x;
end

% Finally get convex hull 
k = convhull(x ,DH, 'simplify', true);
        

end
