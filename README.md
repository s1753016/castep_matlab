Here is a suite of matlab tools to import and analyse castep output data.

The various functions include:

- Import coordinates and unit cell form *.cell files
- Import MD trajectories from *.xyz files
- List all X-Y pair distances and angles
- Calculate pair distribution functions
- Visualise *.cell files
- Compress *.castep files to smaller files
- Generate a binary convex hull with molecular or atomic end points

#### Add directories:
`import	`		`ref`     `external`
`cell_plot`		`main`
to path.

## Examples:

1) Compress all .castep files to .summ files

Change directory to ./Examples/Example1/
`./castep-out` contains a list of .castep files,
```
Castep_summary_loop('castep-out/*.castep')
```
will produce a .summ file for each .castep file in `./castep-out`

Argument works like `ls`, so 
```
Castep_summary_loop('castep-out/CH4*.castep')
```
will convert the files starting with CH4. 

2) Calculate convex hull for .castep data

Change directory to ./Examples/Example2/
Compress the dataset `Castep_summary_loop('castep-out/*.castep')`
Open and run `Example2.m`


3) Get bonds distances and angles from .cell file


Change directory to ./Examples/Example3/
Open and run `Example3.m`
The variable `out` now contains 6 H-C-H bond angles for each CH4

4) Get pair distribution function from .xyz MD data

Change directory to ./Examples/Example4/
Example3.xyz is 12 timesteps of CH4-H2O MD data.
Example3.cell is the corresponding cell file. This is needed for 
correct periodic boundary conditions.
Must have <seed>.xyz and <seed>.cell in the same directory
Open and run `Example4.m`

5) Plot and visualise .cell file

Change directory to ./Examples/Example4/
Open and run `Example5-1.m`
Open and run `Example5-2.m`
