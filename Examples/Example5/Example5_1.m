%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear 
close all
% Load cell
filename='Example5.cell';
[m,Atom,r_cart] = importcell(filename);

global atom_colors atom_sizes u_Atom res
res=200;
u_Atom=unique(Atom);
for i=1:length(u_Atom)
    % Look up atom sizes and colours- 0 for VESTA default, 1 for custom
    tmp = periodic_table(u_Atom(i),0);
    atom_colors(i,:) = tmp{6};
    atom_sizes(i) = tmp{5};
    Atom_n(Atom==u_Atom(i)) = i;
end

% Set up figure
g=get(gca);
axis equal
axis vis3d
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Script goes here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


plot_unit_cell(m)
% h is an array of surface structs for each ion
h = plot_ions(m,Atom_n,r_cart);
% Plot H1-H2 bonds where H2 is in unit cell 
% bond distance min =0.5, max = 1
% Bond style is a cylinder
plot_bonds(m,Atom_n,r_cart,'H','H',0.5,1,'cylinder',0)
% Plot H1-H2 bonds where H2 is maybe not in unit cell 
% bond distance min =1, max = 1.5
% Bond style is a solid line
plot_bonds(m,Atom_n,r_cart,'H','H',1,1.3,'-',1)
% Define a plane using three points in fractional coordinates
a=[0.0886 0.2 -0.1 0.0886 0.7 -0.1 0.1416 0.2 0.22768];
% Plot the plane in red, alpha at 50%
plot_plane(m,a(1:3),a(4:6),a(7:9),'r',0.5)

view([-3 -1 1])
camlight RIGHT 
axis off



