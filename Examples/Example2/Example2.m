% compress
Castep_summary_loop('./castep-out/*.castep');
% Define the atoms
species = categorical({'C' 'H'}); 
% Define the end point molecules (CH4 + H2)
basis = [1 4 ; 0 2];
% Get the convex hull
[DH, x, k,name,stoich] = Custom_Hulls_summ('./castep-out',species,basis);
% Usually only want hull for negative DH
hull_p=[(DH(k)<=0).*x(k);(DH(k)<=0).*(DH(k))]';
% Plot Hull
plot(hull_p(:,1),hull_p(:,2),'-ko','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','k')
hold on
% Scatter points
scatter(x,DH,15,'Marker','o','MarkerEdgeColor','r','MarkerFaceColor','r',...
    'MarkerFaceAlpha',1,'MarkerEdgeAlpha',1);

xlabel('(H_2)_x(CH_4)_{1-x}')
ylabel('\Delta H (eV/f.u.)')