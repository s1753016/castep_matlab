% Do bond analysis, taking every timestep 
MD_bond_orientation('Example4.xyz','C','H',1)   
% Load output files
a = load('Example4-C-H_angles_1.dat');
b = load('Example4-C-H_gofr_1.dat');

subplot(2,2,1) % pair distribution
plot(b(:,1),b(:,2))
xlabel('r (Ang.)')
ylabel('g_{CH}(r)')

subplot(2,2,2) % coordination number
plot(b(:,1),b(:,3))
xlabel('r (Ang.)')
ylabel('n_{CH}(r)')

subplot(2,2,3) % Average bond angle for r<r
plot(a(:,1),a(:,2))
xlabel('r (Ang.)')
ylabel('\langle theta_{CH}(r) \rangle')

subplot(2,2,4) % Variance in bond angle for r<r
plot(a(:,1),a(:,2))
xlabel('r (Ang.)')
ylabel('var(theta_{CH}(r))')
