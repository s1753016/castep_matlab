clear
[th,ph,r,m,Atom] = import_cell_bonds('Example3.cell','C','H');
ph=ph*180/pi;
th=th*180/pi;
histogram(r)

tmpn=6;
maxr=1.4;
for l = 1:size(r,1)
    tmpc=0;
    for i = 1:tmpn
        for j = i+1:tmpn
            if max(r(l,i),r(l,j))<maxr
                tmpc = tmpc+1;
                out(l,tmpc) = distance(ph(l,i),th(l,i),ph(l,j),th(l,j));
            end
        end
    end
end

