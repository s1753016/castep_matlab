% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%   Calculates all distances for all species1-species2 
%   
%   Reads .xyz files generated using md2xyz from Castep. 
%   And the .cell file with identical filename to get unit cell info
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

function [th,ph,r,m,Atom,memberlist] = import_md_bonds(filename,species1,species2,step,st,en)
%% Import Data and initialise
clear textprogressbar
j=0;

[m,Atom] = importcell([filename(1:end-4) '.cell']);
fid = fopen(filename, 'rt'); 
s = textscan(fid, '%s','delimiter', '\n'); 

%% Neighbout shell displacement 
hhh = dec2base(0:26,3);
disp_vec = str2num(reshape(hhh',81,1));
disp_vec = reshape(disp_vec,3,27)'-1;

%% Initialise
n_atoms = str2num(s{1}{1});
n_timesteps= (size(s{1},1))/(n_atoms+2);
tmp = sscanf([s{1}{1+2:1+1+n_atoms}],'%s %f %f %f');
atom = char(tmp(1:4:end));
n_spec1 = sum(atom==species1);
n_spec2 = sum(atom==species2);
       
if nargin > 5
    first = ceil(n_timesteps*(st));
    n_timesteps_frac=ceil(n_timesteps*(en-st));
else 
    first = step;
    n_timesteps_frac=n_timesteps;
end
%% For memory limits, only take 100 nearest neighbours - can change if needed
n_nb =min(100,n_spec2*27);
n_nb =min(4,n_spec2*27);
n_nb =min(11,n_spec2*27);

%  Allocate arrays [time, molecule, bond]
th=zeros(ceil(n_timesteps_frac./step)-5,n_spec1,n_nb);
ph=zeros(ceil(n_timesteps_frac./step)-5,n_spec1,n_nb);
r=zeros(ceil(n_timesteps_frac./step)-5,n_spec1,n_nb);

%% If X-X type
xxt=0;
if species1==species2
    xxt=1;
end





%% Scroll through file until end
textprogressbar(sprintf('Getting %s-%s Bonds          : ',species1,species2));
for ii=first+1:step:ceil(n_timesteps*en)
    textprogressbar(round(100*ii/n_timesteps))
    % Find index of start of timestep
    idx1=1+(n_atoms+2)*(ii-1);
    
    % Will break once it reaches the end - lazy way...
    try
    tmp = sscanf([s{1}{idx1+2:idx1+1+n_atoms}],'%s %f %f %f');
    catch
        break      
    end
    %ii
    
    % Organise data from sscanf
    atom = char(tmp(1:4:end));
    xyz  = [tmp(2:4:end),tmp(3:4:end),tmp(4:4:end)];
    % Get coordinates in fractional coordinates
    xtl = xyz/m;
    % Find all species atoms
    xtl_H = xtl(atom==species2,:);
    % Iterate timestep
    j=j+1;
    % Iterate molecules in cell
    k=0;
    % Loop through all X atoms to find nearest H atoms
    for i = xtl(atom==species1,:)'
        % Vector connecting C-H atoms in frac coords
        vec = i' - xtl_H;
        
        % Nearest image when all fractional coordinate components <0.5
        vec(vec> 0.5) = vec(vec> 0.5)-1;
        vec(vec<-0.5) = vec(vec<-0.5)+1;
	
        
        % Add neighbouring shells
        vec_tmp=[];
        for v_tmp = disp_vec'
                vec_tmp = [vec_tmp; vec + v_tmp';];
        end
        
        % Convert to cartesian coords
        vec_xyz=vec_tmp*m;
        % Get squared distances
        dist_xyz = (sum(vec_xyz.^2,2));        
        % Sort
        %[~,I] = sort(dist_xyz);
        [~,I] = mink(dist_xyz,n_nb+1);
        k = k+1;
        %   Ignore self
        if xxt==1
            XH_bonds = vec_xyz(I(2:n_nb+1),:);
            memberlist(j,k,:) = mod(I(2:n_nb+1),sum(atom==species1));
        else
            XH_bonds = vec_xyz(I(1:n_nb),:);
	    memberlist(j,k,:) = mod(I(1:n_nb),sum(atom==species1));
        end
        
        % Convert to spherical 
        [th(j,k,:),ph(j,k,:),r(j,k,:)] = cart2sph(XH_bonds(:,1),XH_bonds(:,2),XH_bonds(:,3));

        
    end

end

textprogressbar('Done')

end
