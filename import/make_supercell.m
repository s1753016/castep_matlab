function [m_ss,Atom_ss,r_cart_ss] = make_supercell(m,Atom,r_cart_orig,ss)

    m_ss=ss*m;
    N=abs(det(ss));
    if mod(N,1) | N==0
        error('Non integer supercell')
    end
    new_atoms = length(Atom)*N;
    list=[];
    for i1=1:max(ss(1),1)
        for i2=1:max(ss(2),1)
            for i3=1:max(ss(3),1)
                for i4=1:max(ss(4),1)
                    for i5=1:max(ss(5),1)
                        for i6=1:max(ss(6),1)
                            for i7=1:max(ss(7),1)
                                for i8=1:max(ss(8),1)
                                    for i9=1:max(ss(9),1)
                                        ss_tmp = [ i1 i2 i3]+ [i4 i5 i6] + [i7 i8 i9];
                                        list =[list; ss_tmp-2];
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    r_cart_ss=[];
    for i=1:N
        r_cart_ss = [r_cart_ss; r_cart_orig + list(i,:)];
    end

    
    
    r_cart_ss = r_cart_ss*m/m_ss;
%     r_cart_ss(r_cart_ss>1) =r_cart_ss(r_cart_ss>1)-1;
%     r_cart_ss(r_cart_ss<0) =r_cart_ss(r_cart_ss<0)+1;
    
    Atom_ss=repmat(Atom,N,1);
%    size(Atom_ss)
%    size(r_cart_orig)
%    size(r_cart_ss)
%    pause


end
