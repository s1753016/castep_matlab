function file = read_summ(filename)

    t = textread(filename,'%s','delimiter','\n');
    file.Enth = sscanf(t{3},'Enth %f');
    file.n_ions = sscanf(t{4},'N_ions %d');
    file.Volume = sscanf(t{5},'Vol    %f');
    
    file.ions.species = split(t{6});
    file.ions.n = split(t{6});

    file.ions.species = file.ions.species(1:end-1);
    file.ions.n = file.ions.n(1:end-1);
        
    file.name = t{1};




end