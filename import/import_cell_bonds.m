%  Import bond data from a *.cell file. 
% 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

function [th,ph,r,m,Atom] = import_cell_bonds(filename,species1,species2)

%% Import cell data
[m,Atom,r_cart] = importcell(filename);


%% shell displacement
hhh = dec2base(0:26,3);
disp_vec = str2num(reshape(hhh',81,1));
disp_vec = reshape(disp_vec,3,27)'-1;

%% Initialise
n_atoms = size(Atom,1);
n_spec1 = sum(Atom==species1);
n_spec2 = sum(Atom==species2);
       
%% For memory limits, only take 100 nearest neighbours - can change if needed
n_nb =min(100,n_spec2*27-1);

th=zeros(n_spec1,n_nb);
ph=zeros(n_spec1,n_nb);
r=zeros(n_spec1,n_nb);

% If X-X type
xxt=0;
if species1==species2
    xxt=1;
end


xtl = r_cart;
% Find all species2 atoms
xtl_H = xtl(Atom==species2,:);
% Iterate molecules in cell
k=0;
% Loop through all X atoms to find nearest H atoms
for i = xtl(Atom==species1,:)'
    % vec = vector connecting C-H atoms in frac coords
    vec = i' - xtl_H;

    % Nearest image when all fractional coordinate components <0.5
    vec(vec> 0.5) = vec(vec> 0.5)-1;
    vec(vec<-0.5) = vec(vec<-0.5)+1;

    % Add neighbouring shells
    vec_tmp=[];
    for v_tmp = disp_vec'
            vec_tmp = [vec_tmp; vec + v_tmp';];
    end

    % convert to cartesian coords
    vec_xyz=vec_tmp*m;

    % Calculate Distances
    dist_xyz = (sum(vec_xyz.^2,2));

    %   Make list of all C-H distances        
    XH_bonds=zeros(n_nb,3);

    %   Sort
    [~,I] = sort(dist_xyz);
    %   Ignore self
    if xxt==1
        XH_bonds = vec_xyz(I(2:n_nb+1),:);
    else
        XH_bonds = vec_xyz(I(1:n_nb),:);
    end

    k = k+1;
    % Get bonds in spherical coordinates
    [th(k,:),ph(k,:),r(k,:)] = cart2sph(XH_bonds(:,1),XH_bonds(:,2),XH_bonds(:,3));

end

end






