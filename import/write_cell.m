function write_cell(m,Atom,r_cart,fname)
fid = fopen([fname  '.cell'],'w');

fprintf(fid,'%%BLOCK LATTICE_CART\n');
fprintf(fid,'%f %f %f\n',m');
fprintf(fid,'%%ENDBLOCK LATTICE_CART\n');

fprintf(fid,'%%BLOCK POSITIONS_FRAC\n');


for i = 1:size(r_cart,1)
    fprintf(fid,'%s  %f %f %f \n', Atom(i), r_cart(i,:));    
end

fprintf(fid,'%%ENDBLOCK POSITIONS_FRAC\n\n');
fclose(fid);



end
