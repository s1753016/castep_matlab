function [m,Atom,r_cart] = importcell(filename)
    t = textread(filename','%s','delimiter','\n');

    pos1= find(strcmpi(t, '%BLOCK positions_frac'));
    pos2= find(strcmpi(t, '%ENDBLOCK positions_frac'));
    lat1= find(strcmpi(t, '%BLOCK lattice_cart'));
    lat2= find(strcmpi(t, '%ENDBLOCK lattice_cart'));
    fid = fopen('tmp_atom','w');
    for i=t(pos1-1:pos2+1)'
        fprintf(fid,[cell2mat(i) '\n']);
    end
    

    [Atom,x_cart,y_cart,z_cart] = importcell_pos('tmp_atom');
    fclose(fid);

    delete('tmp_atom');
    r_cart=[x_cart,y_cart,z_cart];
    
    fid = fopen('tmp_lat','w');
    for i=t(lat1+1:lat2-1)'
        fprintf(fid,[cell2mat(i) '\n']);
    end
    
    
    [a,b,c] = importcell_lat('tmp_lat');

    fclose(fid);
    delete('tmp_lat');
    
    a = a(~isnan(a)); b = b(~isnan(b)); c = c(~isnan(c)); 
    m = [a,b,c];
end
