
function [xyz,atom,m] = import_xyz(filename)
clear textprogressbar
fid = fopen(filename, 'rt'); 
s = textscan(fid, '%s','delimiter', '\n'); 

%% Initialise
n_atoms = str2num(s{1}{1});
n_timesteps= (size(s{1},1))/(n_atoms+2);
tmp = sscanf([s{1}{1+2:1+1+n_atoms}],'%s %f %f %f');
atom = char(tmp(1:4:end));
       

[m] = importcell([filename(1:end-4) '.cell']);
%% Scroll through file until end
textprogressbar(sprintf('Reading XYZ data             : '));
xyz=zeros(n_timesteps,n_atoms,3);
for ii=1:ceil(n_timesteps)
    textprogressbar(round(100*ii/n_timesteps))
    % Find index of start of timestep
    idx1=1+(n_atoms+2)*(ii-1);
    
    % Will break once it reaches the end - lazy way...
    try
    tmp = sscanf([s{1}{idx1+2:idx1+1+n_atoms}],'%s %f %f %f');
    catch
        break      
    end
    atom = char(tmp(1:4:end));
    xyz(ii,:,:)  = [tmp(2:4:end),tmp(3:4:end),tmp(4:4:end)]/m;
end
%CONVERTING TO ANGSTROM
%xyz = xyz .* 0.529177211;
    
end
